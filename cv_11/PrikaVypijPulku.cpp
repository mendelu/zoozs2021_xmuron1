//
// Created by xmuron1 on 02.12.2021.
//

#include "PrikaVypijPulku.h"

PrikaVypijPulku::PrikaVypijPulku() : Prikaz("Vypije pulku lektvaru") {}

void PrikaVypijPulku::pouzijLektvar(Hrdina *hrdina, Lektvar *lektvar) {
    int kolik = lektvar->getBonus()/2;
    hrdina->pridejZivoty(kolik);
    lektvar->setBonus(kolik);
}