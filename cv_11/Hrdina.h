//
// Created by xmuron1 on 02.12.2021.
//

#ifndef CV_11_HRDINA_H
#define CV_11_HRDINA_H
#include "Lektvar.h"
#include <iostream>
#include <vector>

class Prikaz;

class Hrdina {
private:
    int m_zivoty;
    std::vector<Lektvar*> m_lektvary;
    std::vector<Prikaz*> m_prikazy;
public:
    Hrdina(int zivoty);
    void seberLekvar(Lektvar* lektvar);
    int getZivoty();
    void pridejZivoty(int kolik);
    void naucSePrikaz(Prikaz* prikaz);
    void provedOperaciSLekvarem();


};


#endif //CV_11_HRDINA_H
