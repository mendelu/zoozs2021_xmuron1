#include <iostream>
#include "Hrdina.h"
#include "PrikaVypijPulku.h"
#include "PrikazVypijVse.h"

int main() {
    Hrdina* h = new Hrdina(100);
    h->seberLekvar(new Lektvar("Pivo",100));
    h->seberLekvar(new Lektvar("Voda",10));
    h->naucSePrikaz(new PrikaVypijPulku());
    h->naucSePrikaz(new PrikazVypijVse());
    h->provedOperaciSLekvarem();

    return 0;
}
