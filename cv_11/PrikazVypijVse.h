//
// Created by xmuron1 on 02.12.2021.
//

#ifndef CV_11_PRIKAZVYPIJVSE_H
#define CV_11_PRIKAZVYPIJVSE_H
#include "Prikaz.h"

class PrikazVypijVse : public Prikaz {
public:
    PrikazVypijVse();
    void pouzijLektvar(Hrdina* hrdina, Lektvar* lektvar);
};


#endif //CV_11_PRIKAZVYPIJVSE_H
