//
// Created by xmuron1 on 02.12.2021.
//

#ifndef CV_11_LEKTVAR_H
#define CV_11_LEKTVAR_H
#include <iostream>

class Lektvar {
private:
    int m_bonus;
    std::string m_popis;

public:
    Lektvar(std::string popis, int bonus);
    void printInfo();
    int getBonus();
    void setBonus(int bonus);
};


#endif //CV_11_LEKTVAR_H
