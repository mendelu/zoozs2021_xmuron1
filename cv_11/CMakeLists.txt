cmake_minimum_required(VERSION 3.19)
project(cv_11)

set(CMAKE_CXX_STANDARD 14)

add_executable(cv_11 main.cpp Hrdina.h Hrdina.cpp Lektvar.cpp Lektvar.h Prikaz.cpp Prikaz.h PrikazVypijVse.cpp PrikazVypijVse.h PrikaVypijPulku.cpp PrikaVypijPulku.h)