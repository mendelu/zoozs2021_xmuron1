//
// Created by xmuron1 on 02.12.2021.
//

#ifndef CV_11_PRIKAZ_H
#define CV_11_PRIKAZ_H
#include <iostream>
#include "Hrdina.h"
#include "Lektvar.h"

class Prikaz {
private:
    std::string m_popis;
public:
    Prikaz(std::string popis);
    std::string getPopis();
    virtual void pouzijLektvar(Hrdina* hrdina, Lektvar* lektvar) = 0;

};



#endif //CV_11_PRIKAZ_H
