//
// Created by xmuron1 on 02.12.2021.
//

#include "PrikazVypijVse.h"

PrikazVypijVse::PrikazVypijVse(): Prikaz("Vypije cely lektvar najednou") {

}

void PrikazVypijVse::pouzijLektvar(Hrdina *hrdina, Lektvar *lektvar) {
    hrdina->pridejZivoty(lektvar->getBonus());
    lektvar->setBonus(0);
}