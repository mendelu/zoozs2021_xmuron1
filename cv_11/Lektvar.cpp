//
// Created by xmuron1 on 02.12.2021.
//

#include "Lektvar.h"
Lektvar::Lektvar(std::string popis, int bonus) {
    m_popis = popis;
    m_bonus = bonus;
}

int Lektvar::getBonus() {
    return m_bonus;
}

void Lektvar::setBonus(int bonus) {
    m_bonus -= bonus;
}

void Lektvar::printInfo() {
    std::cout << "Lektvar" << m_popis << " s bonusem " << getBonus() << std::endl;
}

