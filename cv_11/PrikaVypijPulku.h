//
// Created by xmuron1 on 02.12.2021.
//

#ifndef CV_11_PRIKAVYPIJPULKU_H
#define CV_11_PRIKAVYPIJPULKU_H
#include "Prikaz.h"

class PrikaVypijPulku : public Prikaz {
public:
    PrikaVypijPulku();
    void pouzijLektvar(Hrdina* hrdina, Lektvar* lektvar);
};


#endif //CV_11_PRIKAVYPIJPULKU_H
