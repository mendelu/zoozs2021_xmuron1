//
// Created by xmuron1 on 02.12.2021.
//

#include "Hrdina.h"
#include "Prikaz.h"

Hrdina::Hrdina(int zivoty) {
    m_zivoty = zivoty;
}

int Hrdina::getZivoty() {
    return m_zivoty;
}

void Hrdina::pridejZivoty(int kolik) {
    m_zivoty += kolik;
}
void Hrdina::seberLekvar(Lektvar *lektvar) {
    m_lektvary.push_back(lektvar);
}

void Hrdina::naucSePrikaz(Prikaz *prikaz) {
    m_prikazy.push_back(prikaz);
}

void Hrdina::provedOperaciSLekvarem() {
    // vyptame si jaky lektvar
    std::cout << "S jakym lektvarem chces pracovat? " << std::endl;
    int i = 0;
    for(Lektvar* l : m_lektvary){
        std::cout << i << ")" << l->getBonus() << std::endl;
        i += 1;
    }
    int index_lektvaru;
    std::cin >> index_lektvaru;
    Lektvar* lektvar = m_lektvary[index_lektvaru];

    // vyptame si jaky prikaz
    std::cout << "Jaky prikaz chces pouzit? " << std::endl;
    int j = 0;
    for(Prikaz* p : m_prikazy){
        std::cout << j << ")" << p->getPopis() << std::endl;
        j += 1;
    }
    int index_prikazu;
    std::cin >> index_prikazu;
    Prikaz* prikaz = m_prikazy[index_prikazu];

    // aplikuj prikaz nad lektvarem
    prikaz->pouzijLektvar(this,lektvar);
}