//
// Created by xmuron1 on 11.11.2021.
//

#ifndef VESMIR_GAMEENGINE_H
#define VESMIR_GAMEENGINE_H
#include "HraciDeska.h"
#include "Planeta.h"
#include "Asteroid.h"
class GameEngine {
private:
    HraciDeska* m_hraci_deska;
    void vypisNapovedu();
public:
    GameEngine();
    void hrej();
};


#endif //VESMIR_GAMEENGINE_H
