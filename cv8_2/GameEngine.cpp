//
// Created by xmuron1 on 11.11.2021.
//

#include "GameEngine.h"

GameEngine::GameEngine() {
    m_hraci_deska = new HraciDeska(2,2);
    m_hraci_deska->vloz(0,0,new Planeta("Merkur",151));
    m_hraci_deska->vloz(0,1,new Planeta("Venuse",1000));
    m_hraci_deska->vloz(1,0,new Asteroid("A11"));
    m_hraci_deska->vloz(1,1,new Planeta("Saturn",560));
}

void GameEngine::vypisNapovedu() {
    std::cout << "[0] konec" << std::endl;
    std::cout << "[1] vypis napovedu" << std::endl;
    std::cout << "[2] zobraz mapu" << std::endl;
    std::cout << "[3] tezit z pozice" << std::endl;
    // TODO pridat tezbu...
}

void GameEngine::hrej() {
    vypisNapovedu();
    int rozhodnuti = 0;
    do {
        std::cout << "Co chces udelat?" << std::endl;
        std::cin >> rozhodnuti;

        // TODO predelat na switch
        if(rozhodnuti == 1){
            vypisNapovedu();
        }
        if(rozhodnuti == 2){
            m_hraci_deska->vypisDesku();
        }
        if(rozhodnuti == 3){
            int x;
            int y;
            std::cout << "Zadej radek" << std::endl;
            std::cin >> x;
            std::cout << "Zadej sloupec" << std::endl;
            std::cin >> y;

            int vytezeno = m_hraci_deska->vrat(x,y)->vytezDavkuRudy();
            std::cout << "Vytezil si " << vytezeno << " rudy." << std::endl;
        }

    } while (rozhodnuti != 0);
}
// TODO metoda hrej
// konec 0
// vypis napovedu 1
// vypis mapu 2