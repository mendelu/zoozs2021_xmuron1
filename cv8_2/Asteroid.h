//
// Created by xmuron1 on 11.11.2021.
//

#ifndef VESMIR_ASTEROID_H
#define VESMIR_ASTEROID_H
#include "HerniPole.h"

class Asteroid : public HerniPole{
public:
    Asteroid(std::string popis);
    int vytezDavkuRudy();
};


#endif //VESMIR_ASTEROID_H
