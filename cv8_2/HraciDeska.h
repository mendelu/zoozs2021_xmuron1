//
// Created by Mikulas Muron on 11/11/2021.
//

#ifndef MOJE_VESMIRNA_ADVENTURA_HRACIDESKA_H
#define MOJE_VESMIRNA_ADVENTURA_HRACIDESKA_H

#include <iostream>
#include <vector>
#include "HerniPole.h"

class HraciDeska {
private:
    std::vector<std::vector< HerniPole*> > m_deska;
public:
    /// Konstruktor, ketry vytvori prazdne pole o predane velikosti
    HraciDeska(int pocetRadek, int pocetSloupcu);
    /// Pokud je to mozne, vrati objekt, ktery je na dane pozici v herni desce
    HerniPole* vrat(int radek, int sloupec);
    /// Vlozi novy objekt, ktery je potomkem tridy HraciPole na pozici v desce
    void vloz(int radek, int sloupec, HerniPole* pole);
    /// Vypise popisy jednotlivych hracich poli na obrazovku
    void vypisDesku();

    // TODO
    // void vymazDesku();

};


#endif //MOJE_VESMIRNA_ADVENTURA_HRACIDESKA_H
