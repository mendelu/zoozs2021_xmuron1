//
// Created by Mikulas Muron on 11/11/2021.
//

#ifndef MOJE_VESMIRNA_ADVENTURA_HERNIPOLE_H
#define MOJE_VESMIRNA_ADVENTURA_HERNIPOLE_H

#include <iostream>

/**
     * @brief Abstraktni trida predstavujici obecne herni pole
     *
     * Tato trida predstavuje obecne hraci pole. Toto pole se pouziva pouze pro
     * dedeni ruznych konkretnich typu hracich poli. Slouzi hlavne k tomu, abychom
     * mohli mit univerzalni ukazatel na libovolne hraci pole, dosadili si za nej
     * co uzname za vhodne a pak provolavali ruzne implementace metod (polym.).
     * Vytvarejte si vlastni potomky a doplnujte atributy a metody podle potreby.
*/

class HerniPole {
private:
    /// Atribut, ktery se dedi do vsech potomku
    std::string m_popis;
public:
    HerniPole(std::string popis);
    // Metoda, ktera se dedi do vsech potomku
    std::string getPopis();
    /**
         * @brief Metoda vracejici kolik rudy lze z lokace vytezit
         *
         * Ilustruje polymorfni chovani poli na hraci desce - kazde pole nyni musi
		 * vracet obr. hodnotu vypoctenou podle nejakeho kriteria - pro planetu se
		 * pocita jinak, pro asteroid jinak atp.
         * Pokud by existovala nejaka univerzalni metoda vypoctu, kterou by pouzivala
         * vetsina poli, tak bychom ji udelali pouze virtualni a pouze v nekterem
         * potomkovi prekryli.
         */
    virtual int vytezDavkuRudy() = 0;
    /// Tridy, ktere budou mit potomky by mely mit virt. dest., pak se zavola vzdy destruktor spravneho potomka
    virtual ~HerniPole(){};
};


#endif //MOJE_VESMIRNA_ADVENTURA_HERNIPOLE_H
