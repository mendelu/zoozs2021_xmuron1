//
// Created by xmuron1 on 11.11.2021.
//

#ifndef MOJE_VESMIRNA_ADVENTURA_PLANETA_H
#define MOJE_VESMIRNA_ADVENTURA_PLANETA_H
#include "HerniPole.h"

class Planeta : public HerniPole {
private:
    int m_mnozstviRudy;
public:
    Planeta(std::string popis, int ruda);
    int vytezDavkuRudy();
};


#endif //MOJE_VESMIRNA_ADVENTURA_PLANETA_H
