cmake_minimum_required(VERSION 3.19)
project(cv6)

set(CMAKE_CXX_STANDARD 14)

add_executable(cv6 main.cpp)