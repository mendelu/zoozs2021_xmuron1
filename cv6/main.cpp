#include <iostream>
#include <array>
using namespace std;

class Kontejner{
private:
    int m_vaha;
    string m_obsah;
    string m_majitel;
public:
    Kontejner(int vaha, string obsah, string majitel){
        // TODO kontrola vstupu
        m_vaha = vaha;
        m_obsah = obsah;
        m_majitel = majitel;
    }

    int getVaha(){
        return m_vaha;
    }

    string getObsah(){
        return m_obsah;
    }

    string getMajitel(){
        return m_majitel;
    }

    void printInfo(){
        cout << "Jsem Kontejer s vahou " << getVaha() << " mam v sobe " << getObsah() << " vlastni me " << getMajitel() << endl;
    }
};

class Patro{
private:
    string m_oznaceni;
    array<Kontejner*,10> m_pozice;
public:
    Patro(string oznaceni){
        m_oznaceni = oznaceni;
        for(Kontejner* &k:m_pozice){
            k = nullptr;
        }
    }

    void vypisObsah(){
        for(Kontejner* k:m_pozice){
            if(k != nullptr) {
                k->printInfo();
            }
            else{
                cout << "Na tehle pozici nic neni" << endl;
            }
        }
    }

    void uloz(Kontejner* kontejner, int pozice){
        // TODO radeji .at()
        if(m_pozice[pozice] == nullptr) {
            m_pozice[pozice] = kontejner;
        }
        else{
            cout << "Pozice je jiz obsazena" << endl;
        }
    }

    void odeber(int pozice){
        // TODO radeji .at()
        if(m_pozice[pozice] != nullptr) {
            m_pozice[pozice] = nullptr;
        }
        else{
            cout << "Na pozici nic neni, nelze nic odebrat" << endl;
        }
    }
};

class Sklad{
private:
    array<Patro*,5> m_patra;
public:
    // TODO pridat desktruktor
    Sklad(){
        for(Patro* &p:m_patra){
            // TODO dynamicky generovat ozaceni patra
            p = new Patro("1");
        }
    }

    void ulozKontejner(int index_patra, int index_pozice_v_patre, Kontejner* k){
        m_patra[index_patra]->uloz(k, index_pozice_v_patre);
    }

    void odeberKontejner(int index_patra, int index_pozice_v_patre){
        m_patra[index_patra]->odeber(index_pozice_v_patre);
    }

    void vypisObsah(){
        // TODO lepsi vypis
        for(Patro* p:m_patra){
            p->vypisObsah();
        }
    }
};


int main() {
    Sklad* sklad = new Sklad();
    Kontejner* k1 = new Kontejner(1000,"zbozi","Mikulas");
    Kontejner* k2 = new Kontejner(2000,"odpadky","Mikulas");
    sklad->ulozKontejner(0,0,k1);
    sklad->ulozKontejner(1,0,k2);
    sklad->vypisObsah();
    return 0;
}
