//
// Created by xmuron1 on 11.11.2021.
//

#include "Planeta.h"

Planeta::Planeta(std::string popis, int mnozstviRudy) : HerniPole(popis) {
    m_mnozstviRudy = mnozstviRudy;
}

int Planeta::vytezDavkuRudy() {
    int ruda = m_mnozstviRudy * 0.1;
    m_mnozstviRudy -= ruda;
    return ruda;
}