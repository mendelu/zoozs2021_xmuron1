//
// Created by Mikulas Muron on 11/11/2021.
//

#include "HerniPole.h"


HerniPole::HerniPole(std::string popis) {
    m_popis = popis;
}

std::string HerniPole::getPopis() {
    return m_popis;
}