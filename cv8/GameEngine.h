//
// Created by xmuron1 on 11.11.2021.
//

#ifndef MOJE_VESMIRNA_ADVENTURA_GAMEENGINE_H
#define MOJE_VESMIRNA_ADVENTURA_GAMEENGINE_H

#include "HraciDeska.h"
#include "Planeta.h"
#include "Asteroid.h"
#include <iostream>

class GameEngine {
private:
    HraciDeska* m_hraciDeska;
    void vypisHelp();
public:
    GameEngine();
    void hrej();
};


#endif //MOJE_VESMIRNA_ADVENTURA_GAMEENGINE_H
