cmake_minimum_required(VERSION 3.15)
project(moje_vesmirna_adventura)

set(CMAKE_CXX_STANDARD 14)

add_executable(vesmir main.cpp HerniPole.cpp HerniPole.h HraciDeska.cpp HraciDeska.h Planeta.cpp Planeta.h GameEngine.cpp GameEngine.h Asteroid.cpp Asteroid.h)