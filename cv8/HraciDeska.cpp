//
// Created by Mikulas Muron on 11/11/2021.
//

#include "HraciDeska.h"


HraciDeska::HraciDeska(int pocetRadek, int pocetSloupcu) {
    for(int radek=0;radek<pocetRadek;radek++){
        std::vector<HerniPole*> pomocny_radek;
        for(int sloupec=0;sloupec<pocetSloupcu;sloupec++){
            pomocny_radek.push_back(nullptr);
        }
        m_deska.push_back(pomocny_radek);
    }
    // nebo taky takto
    /*
    std::vector<HerniPole*> pomocnyRadek(pocetSloupcu, 0);
    // lze samozrejme i jinak
    m_deska.resize(pocetRadek);
    for(int i=0; i<pocetRadek; i++){
        m_deska[i] = pomocnyRadek;
    }
     */
}

void HraciDeska::vloz(int radek, int sloupec, HerniPole* pole) {
    // TODO kontrola rozsahu pro radek/sloupec
    m_deska[radek][sloupec] = pole;
}

HerniPole * HraciDeska::vrat(int radek, int sloupec) {
    // TODO kontrola rozsahu pro radek/sloupec
    // TODO kontrola, zda na pozici neco je
    return m_deska[radek][sloupec];
}

void HraciDeska::vypisDesku() {
    for(int radek=0; radek<m_deska.size(); radek++) {
        // TODO kontrola na prvni sloupec
        for (int sloupec = 0; sloupec < m_deska[0].size(); sloupec++) {
            // TODO kontrola, zda na pozici neco je
            std::cout << m_deska[radek][sloupec]->getPopis() << "\t \t ";
        }
        std::cout << std::endl;
    }
}