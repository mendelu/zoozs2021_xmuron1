//
// Created by xmuron1 on 11.11.2021.
//

#ifndef MOJE_VESMIRNA_ADVENTURA_ASTEROID_H
#define MOJE_VESMIRNA_ADVENTURA_ASTEROID_H
#include "HerniPole.h"
#include <iostream>

class Asteroid : public HerniPole {
public:
    Asteroid(std::string popis);
    int vytezDavkuRudy();
};


#endif //MOJE_VESMIRNA_ADVENTURA_ASTEROID_H
