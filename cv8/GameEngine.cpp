//
// Created by xmuron1 on 11.11.2021.
//

#include "GameEngine.h"


GameEngine::GameEngine() {
    m_hraciDeska = new HraciDeska(2,2);
    m_hraciDeska->vloz(0,0,new Planeta("Merkur",100));
    m_hraciDeska->vloz(0,1,new Planeta("Jupyter",500));
    m_hraciDeska->vloz(1,0, new Planeta("Zeme",6));
    m_hraciDeska->vloz(1,1, new Asteroid("A11"));
}

void GameEngine::vypisHelp() {
    std::cout << "[0] konec" << std::endl;
    std::cout << "[1] pro help" << std::endl;
    std::cout << "[2] pro vypsani mapy" << std::endl;
    std::cout << "[3] vytez" << std::endl;
    // TODO dalsi operace
}

void GameEngine::hrej() {
    vypisHelp();
    int rozhodnuti = 0;
    do {
        std::cout << "Co chces udelat?" << std::endl;
        std::cin >> rozhodnuti;

        // TODO predelat na switch
        // vypis moznosti (help)
        if(rozhodnuti == 1){
            vypisHelp();
        }
        if(rozhodnuti == 2){
            m_hraciDeska->vypisDesku();
        }
        if(rozhodnuti == 3){
            int x;
            int y;
            std::cout << "Zadej radek" << std::endl;
            std::cin >> x;
            std::cout << "Zadej sloupec" << std::endl;
            std::cin >> y;
            int vytezeno = m_hraciDeska->vrat(x,y)->vytezDavkuRudy();
            std::cout << "Vytezil si " << vytezeno << " kusu rudy" << std::endl;
        }


    } while (rozhodnuti != 0);
}
