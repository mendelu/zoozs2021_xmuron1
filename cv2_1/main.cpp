#include <iostream>
using namespace std;
class Student{
public:
    string m_jmeno;
    string m_rc;
    string m_bydliste;

    void printInfo(){
        cout << "Jsem student " << getJmeno();
        cout << " s RC" << getRC();
        cout << " a bydlim na " << getBydliste() << endl;
    }

    Student(string jmeno, string rc, string adresa){
        m_jmeno = jmeno;
        m_rc = rc;
        setBydliste(adresa);
    }

    // delegovani konst.
    Student(string jmeno, string rc) : Student(jmeno, rc, "bydliste nezadano"){

    }
    // puvodni konst
    /*
    Student(string jmeno, string rc){
        m_jmeno = jmeno;
        m_rc = rc;
        setBydliste("bydliste nezadano");
    }
     */

    void setBydliste(string bydliste) {
        m_bydliste = bydliste;
    }

    string getRC(){
        return m_rc;
    }

    string getJmeno(){
        return m_jmeno;
    }

    string getBydliste(){
        return m_bydliste;
    }


};

int main() {
    Student* s = new Student("Mikulas","910310/6516015");
    Student* d = new Student("Pepa","446/165","Krokova 12");
    s->setBydliste("blalaba");
    s->printInfo();
    return 0;
}
