#ifndef LEHKOODENECBUILDER_H
#define LEHKOODENECBUILDER_H
#include <iostream>
#include "RytirBuilder.h"
#include "LehkaUtocnaHelma.h"
#include "KrouzkoveBrneni.h"

using namespace std;

namespace rytiri {
	class LehkoOdenecBuilder : public rytiri::RytirBuilder {


	public:
		void buildHelma(int velikost);

		void buildBrneni(int vaha, int odolnost, int ohebnost, int velikostPlatu);
	};
}
#endif // LEHKOODENECBUILDER_H

