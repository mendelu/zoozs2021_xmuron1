TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    LehkejednotkyFactory.cpp \
    LehkoOdenec.cpp \
    Rytir.cpp \
    TezkejednotkyFactory.cpp \
    TezkoOdenec.cpp

HEADERS += \
    JednotkyFactory.h \
    KouzelnikFactory.h \
    LehkejednotkyFactory.h \
    LehkoOdenec.h \
    Rytir.h \
    TezkejednotkyFactory.h \
    TezkoOdenec.h
