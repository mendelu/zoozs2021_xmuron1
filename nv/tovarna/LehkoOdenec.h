#ifndef LehkoOdenec_H
#define LehkoOdenec_H

#include "Rytir.h"

namespace rytiri {
	class LehkoOdenec : public rytiri::Rytir {

	public:
		int m_vahaHelmy;

		LehkoOdenec(string jmeno, int sila, int vahaHelmy);

		int getUtok();

		int getObrana();

		void setVahaHelmy(int silaHelmy);
	};
}
#endif // LehkoOdenec_H
