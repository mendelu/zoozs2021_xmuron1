#include <iostream>
#include "Engine.h"

using namespace std;

Engine* Engine::s_engine = nullptr;

int main()
{
    Engine* engine = Engine::getInstance();

    engine->addPlaneta("Merkur");
    engine->addPlaneta("Zeme");
    engine->addPlaneta("Jupiter");

    engine->vypisInfo();

    delete engine;

    return 0;
}
