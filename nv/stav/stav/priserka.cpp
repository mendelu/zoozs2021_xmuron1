#include "priserka.h"

Priserka::Priserka()
{
    m_stav = new Zaba();
}

Priserka::~Priserka(){
    delete m_stav;
}

void Priserka::polibek(){
    delete m_stav;
    m_stav = new Princ();
}

void Priserka::privitej(){
    m_stav->privitej();
}

void Priserka::rozlucSe(){
    m_stav->rozlucSe();
}
