#ifndef PRISERKA_H
#define PRISERKA_H
#include "zaba.h"
#include "princ.h"

class Priserka
{
private:
    Stav* m_stav;
public:
    Priserka();
    ~Priserka();
    void polibek();
    void privitej();
    void rozlucSe();
};

#endif // PRISERKA_H
