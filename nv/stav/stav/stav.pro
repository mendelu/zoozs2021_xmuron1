TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    priserka.cpp \
    stav.cpp \
    princ.cpp \
    zaba.cpp

HEADERS += \
    priserka.h \
    stav.h \
    princ.h \
    zaba.h
