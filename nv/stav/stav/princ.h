#ifndef PRINC_H
#define PRINC_H
#include "stav.h"

class Princ: public Stav
{
public:
    Princ();
    ~Princ() { };
    void privitej();
    void rozlucSe();
};

#endif // PRINC_H
