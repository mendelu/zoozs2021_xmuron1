#ifndef STAV_H
#define STAV_H
#include <iostream>

class Stav
{
public:
    Stav() { };
    virtual ~Stav() { };
    virtual void privitej() = 0;
    virtual void rozlucSe() = 0;
};

#endif // STAV_H
