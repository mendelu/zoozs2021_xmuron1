#ifndef __Zakaznik_h__
#define __Zakaznik_h__

#include <iostream>

namespace is
{
	class Zakaznik
	{
    private:
        std::string m_jmeno;
		int m_id;

    public:
        Zakaznik(std::string jmeno, int id);
        int getId();
        std::string getJmeno();
	};
}

#endif
