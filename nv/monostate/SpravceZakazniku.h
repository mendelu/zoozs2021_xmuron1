#ifndef __SpravceZakazniku_h__
#define __SpravceZakazniku_h__

#include <vector>
#include <iostream>
#include "Zakaznik.h"

namespace is
{
	class SpravceZakazniku
	{
    private:
        static std::vector<is::Zakaznik*> s_zakaznici;
		static int s_pocitadloZakazniku;
		SpravceZakazniku(){}

    public:
        static is::Zakaznik* getZakaznikById(int id);
		static std::vector<is::Zakaznik*> getZakaznikByName(std::string jmeno);
        static is::Zakaznik* createZakaznik(std::string jmeno);
	};
}

#endif
