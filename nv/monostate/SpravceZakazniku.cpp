#include "SpravceZakazniku.h"

int is::SpravceZakazniku::s_pocitadloZakazniku = 0;
std::vector<is::Zakaznik*> is::SpravceZakazniku::s_zakaznici = {};

is::Zakaznik* is::SpravceZakazniku::getZakaznikById(int id){
    for (Zakaznik* zakaznik:s_zakaznici){
        // zeptam se na id a pokud sedi
        if (zakaznik->getId() == id) {
            // pridam do seznamu vysledku
            return zakaznik;
        }
    }
    return nullptr;
}

std::vector<is::Zakaznik*> is::SpravceZakazniku::getZakaznikByName(std::string jmeno){
    std::vector<is::Zakaznik*> vysledky;

    // projdu vektor zakazniku
    for (int i=0; i<s_zakaznici.size(); i++){
        // zeptam se na jmeno a pokud sedi
        if (s_zakaznici.at(i)->getJmeno() == jmeno) {
            // pridam do seznamu vysledku
            vysledky.push_back( s_zakaznici.at(i) );
        }
    }
    return vysledky;
}

is::Zakaznik* is::SpravceZakazniku::createZakaznik(std::string jmeno){
    // vytvorim zakaznika
    Zakaznik* novyZakaznik = new Zakaznik(jmeno, s_pocitadloZakazniku);
    s_pocitadloZakazniku++;
    // vlozit do vektoru
    s_zakaznici.push_back(novyZakaznik);
    // vratit ho
    return novyZakaznik;
}
