#include "Zakaznik.h"

is::Zakaznik::Zakaznik(std::string jmeno, int id) {
    m_jmeno = jmeno;
    m_id = id;
}

int is::Zakaznik::getId() {
	return this->m_id;
}

std::string is::Zakaznik::getJmeno() {
	return this->m_jmeno;
}

