#include "Lektvar.h"

Lektvar::Lektvar(int bonus, string popis) {
    m_bonusZivota = bonus;
    m_popis = popis;
}

void Lektvar::printInfo() {
    cout<<"bonus lektvaru: "<<m_bonusZivota<<", popis: "<<m_popis<<endl;
}

int Lektvar::getBonusZivot() {
    return m_bonusZivota;
}

void Lektvar::setBonusZivot(int kolik) {
    m_bonusZivota = kolik;
}
