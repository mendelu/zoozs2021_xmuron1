TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    Hrdina.cpp \
    Lektvar.cpp \
    PrikazProhledni.cpp \
    PrikazUpijTrochu.cpp \
    PrikazVypijVse.cpp

HEADERS += \
    Hrdina.h \
    Lektvar.h \
    Prikaz.h \
    PrikazVypijVse.h \
    PrikazProhledni.h \
    PrikazUpijTrochu.h
