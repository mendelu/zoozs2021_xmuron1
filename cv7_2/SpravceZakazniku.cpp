//
// Created by xmuron1 on 04.11.2021.
//

#include "SpravceZakazniku.h"

std::vector<Zakaznik*> SpravceZakazniku::s_zakaznici = {};
int SpravceZakazniku::s_pocet_zakazniku = 0;

Zakaznik* SpravceZakazniku::najdiPodleID(int id) {
    for(Zakaznik* z: s_zakaznici){
        if(z->getID() == id){
            return z;
        }
    }
    return vytvorZakaznika("Anonym", id);
}

std::vector<Zakaznik*> SpravceZakazniku::najdiPodleJmena(std::string jmeno) {
    std::vector<Zakaznik*> nalezeni_zakaznici;
    for(Zakaznik* z:s_zakaznici){
        if(z->getJmeno() == jmeno){
            nalezeni_zakaznici.push_back(z);
        }
    }
    return nalezeni_zakaznici;
}


Zakaznik * SpravceZakazniku::vytvorZakaznika(std::string jmeno) {
    Zakaznik* z = new Zakaznik(s_pocet_zakazniku+1, jmeno);
    s_pocet_zakazniku += 1;
    s_zakaznici.push_back(z);
    return z;
}

Zakaznik * SpravceZakazniku::vytvorZakaznika(std::string jmeno, int id) {
    Zakaznik* z = new Zakaznik(id, jmeno);
    s_zakaznici.push_back(z);
    return z;
}