//
// Created by xmuron1 on 04.11.2021.
//

#include "Zakazka.h"


Zakazka::Zakazka(std::string popis, int id_zakaznika) {
    m_cena = 0;
    m_popis = popis;
    m_zakaznik = SpravceZakazniku::najdiPodleID(id_zakaznika);
}

void Zakazka::setCena(int cena) {
    m_cena = cena;
}