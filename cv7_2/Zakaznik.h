//
// Created by xmuron1 on 04.11.2021.
//

#ifndef CV7_2_ZAKAZNIK_H
#define CV7_2_ZAKAZNIK_H
#include <iostream>

class Zakaznik {
private:
    int m_id;
    std::string m_jmeno;
public:
    Zakaznik(int id, std::string jmeno);
    std::string getJmeno();
    int getID();
};


#endif //CV7_2_ZAKAZNIK_H
