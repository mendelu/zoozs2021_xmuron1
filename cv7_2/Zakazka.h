//
// Created by xmuron1 on 04.11.2021.
//

#ifndef CV7_2_ZAKAZKA_H
#define CV7_2_ZAKAZKA_H
#include <iostream>
#include "Zakaznik.h"
#include "SpravceZakazniku.h"

class Zakazka {
private:
    std::string m_popis;
    int m_cena;
    Zakaznik* m_zakaznik;
public:
    Zakazka(std::string popis, int id_zakaznika);
    void setCena(int cena);

};


#endif //CV7_2_ZAKAZKA_H
