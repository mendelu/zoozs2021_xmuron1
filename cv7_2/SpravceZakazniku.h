//
// Created by xmuron1 on 04.11.2021.
//

#ifndef CV7_2_SPRAVCEZAKAZNIKU_H
#define CV7_2_SPRAVCEZAKAZNIKU_H
#include <iostream>
#include <vector>
#include "Zakaznik.h"

class SpravceZakazniku {
private:
    static std::vector<Zakaznik*> s_zakaznici;
    static int s_pocet_zakazniku;
public:
    static Zakaznik* najdiPodleID(int id);
    static Zakaznik* vytvorZakaznika(std::string jmeno);
    static Zakaznik* vytvorZakaznika(std::string jmeno, int id);
    static std::vector<Zakaznik*> najdiPodleJmena(std::string jmeno);
};


#endif //CV7_2_SPRAVCEZAKAZNIKU_H
