#include <iostream>
#include <vector>
#include "Zakaznik.h"
#include "Zakazka.h"
#include "SpravceZakazniku.h"

int main() {
    Zakaznik* zakaznik = SpravceZakazniku::vytvorZakaznika("Mikulas");
    SpravceZakazniku::vytvorZakaznika("Eva");
    SpravceZakazniku::vytvorZakaznika("Petr");
    SpravceZakazniku::vytvorZakaznika("Mikulas");
    Zakazka* z = new Zakazka("", zakaznik->getID());
    return 0;
}
