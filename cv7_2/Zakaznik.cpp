//
// Created by xmuron1 on 04.11.2021.
//

#include "Zakaznik.h"

Zakaznik::Zakaznik(int id, std::string jmeno) {
    m_jmeno = jmeno;
    m_id = id;
}

std::string Zakaznik::getJmeno() {
    return m_jmeno;
}

int Zakaznik::getID() {
    return m_id;
}