#include <iostream>
using namespace std;

class Developer{
private:
    string m_jmeno;
    int m_mzda;
    int m_odpracovano;
    int m_bonus;
    int m_pocetChyb;
    int m_penalizace;

public:
    Developer(string jmeno, int mzda){
        m_jmeno = jmeno;
        m_mzda = mzda;
        m_odpracovano = 0;
        m_bonus = 2*mzda;
        m_pocetChyb = 0;
        m_penalizace = mzda;
    }

    Developer(string jmeno, int mzda, int odpracovano, int bonus, int pocetChyb, int penalizace){
        m_jmeno = jmeno;
        m_mzda = mzda;
        m_odpracovano = odpracovano;
        m_bonus = bonus;
        m_pocetChyb = pocetChyb;
        m_penalizace = penalizace;
    }

    void evidujChybu(int pocet){
        m_pocetChyb += pocet;
    }

    void pracuj(int kolikHodin){
        m_odpracovano = m_odpracovano + kolikHodin;
    }

    void nastavMzdu(int novaMzda){
        m_mzda = novaMzda;
    }

    void novyMesic(){
        m_odpracovano = 0;
        m_pocetChyb = 0;
    }

    void printInfo(){
        cout << "Jsem Developer " << m_jmeno << endl;
        cout << "Odpracoval jsem " << m_odpracovano << " hodin" << endl;
        cout << "Dostanu " << spocitejMzdu() << " pezen." << endl;
        cout << "Udelal jsem " << m_pocetChyb << " chyb" << endl;

    }

    int spocitejMzdu(){
        int plat = 0;
        if(m_odpracovano<40){
            plat = m_odpracovano * m_mzda;
        }
        else{
            plat = 40 * m_mzda;
            plat += (m_odpracovano-40) * m_bonus;
        }

        plat -= m_pocetChyb * m_penalizace;
        if(plat<0){
            plat = 0;
        }
        return plat;
    }

};


int main() {
    Developer* d = new Developer("Mikulas",100);
    d->pracuj(50);
    d->evidujChybu(10);
    d->printInfo();
    d->novyMesic();
    d->printInfo();

    return 0;
}
