//
// Created by xmuron1 on 25.11.2021.
//

#ifndef CV10_2_HRDINA_H
#define CV10_2_HRDINA_H
#include "Brneni.h"
#include "Zbran.h"

class Hrdina {
private:
    int m_zivoty;
    int m_utok;
    Brneni* m_brneni;
    Zbran* m_zbran;
public:
    Hrdina(int utok);
    int getUtok();
    int getObrana();
    void seberBrneni(Brneni* brneni);
    void seberZbran(Zbran* zbran);
};


#endif //CV10_2_HRDINA_H
