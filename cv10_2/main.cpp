#include <iostream>
#include "HrdinaDirector.h"
#include "ChudyHrdinaBuilder.h"
#include "BohatyHrdinaBuilder.h"

int main() {
    HrdinaDirector* d = new HrdinaDirector(new ChudyHrdinaBuilder());
    Hrdina* h1 = d->postavHrdinu();
    Hrdina* h2 = d->postavHrdinu();

    d->setBuilder(new BohatyHrdinaBuilder());
    Hrdina* h3 = d->postavHrdinu();
    return 0;
}
