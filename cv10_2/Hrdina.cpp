//
// Created by xmuron1 on 25.11.2021.
//

#include "Hrdina.h"
Hrdina::Hrdina(int utok) {
    m_zbran = nullptr;
    m_brneni = nullptr;
    m_zivoty = 100;
    m_utok = utok;
}

void Hrdina::seberBrneni(Brneni *brneni) {
    m_brneni = brneni;
}

void Hrdina::seberZbran(Zbran *zbran) {
    m_zbran = zbran;
}

int Hrdina::getUtok() {
    if(m_zbran != nullptr){
        return m_utok + m_zbran->getBonus();
    }
    return m_utok;
}

int Hrdina::getObrana() {
    if(m_brneni != nullptr){
        return m_brneni->getBonus();
    }
    return 0;
}