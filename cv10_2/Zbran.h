//
// Created by xmuron1 on 25.11.2021.
//

#ifndef CV10_2_ZBRAN_H
#define CV10_2_ZBRAN_H


class Zbran {
private:
    int m_bonus_utoku;
public:
    Zbran(int bonus);
    int getBonus();
};


#endif //CV10_2_ZBRAN_H
