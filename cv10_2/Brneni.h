//
// Created by xmuron1 on 25.11.2021.
//

#ifndef CV10_2_BRNENI_H
#define CV10_2_BRNENI_H


class Brneni {
private:
    int m_bonus_obrany;
public:
    Brneni(int bonus);
    int getBonus();
};


#endif //CV10_2_BRNENI_H
