//
// Created by xmuron1 on 25.11.2021.
//

#include "HrdinaBuilder.h"
HrdinaBuilder::HrdinaBuilder() {
    m_hrdina = nullptr;
}

void HrdinaBuilder::vytvorHrdinu() {
    m_hrdina = new Hrdina(10);
}

Hrdina * HrdinaBuilder::getHrdina() {
    return m_hrdina;
}