//
// Created by xmuron1 on 25.11.2021.
//

#ifndef CV10_2_CHUDYHRDINABUILDER_H
#define CV10_2_CHUDYHRDINABUILDER_H
#include "HrdinaBuilder.h"

class ChudyHrdinaBuilder: public HrdinaBuilder{
public:
    ChudyHrdinaBuilder();
    void setZbran() override;
    void setBrneni() override;
};


#endif //CV10_2_CHUDYHRDINABUILDER_H
