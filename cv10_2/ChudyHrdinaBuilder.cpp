//
// Created by xmuron1 on 25.11.2021.
//

#include "ChudyHrdinaBuilder.h"

ChudyHrdinaBuilder::ChudyHrdinaBuilder() : HrdinaBuilder() {

}

void ChudyHrdinaBuilder::setZbran() {
    m_hrdina->seberZbran(new Zbran(1));
}

void ChudyHrdinaBuilder::setBrneni() {
    m_hrdina->seberBrneni(new Brneni(1));
}