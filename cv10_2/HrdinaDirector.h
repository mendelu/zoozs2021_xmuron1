//
// Created by xmuron1 on 25.11.2021.
//

#ifndef CV10_2_HRDINADIRECTOR_H
#define CV10_2_HRDINADIRECTOR_H
#include "HrdinaBuilder.h"

class HrdinaDirector {
private:
    HrdinaBuilder* m_builder;
public:
    HrdinaDirector(HrdinaBuilder* builder);
    Hrdina* postavHrdinu();
    void setBuilder(HrdinaBuilder* builder);
};


#endif //CV10_2_HRDINADIRECTOR_H
