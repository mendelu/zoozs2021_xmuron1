//
// Created by xmuron1 on 25.11.2021.
//

#ifndef CV10_2_HRDINABUILDER_H
#define CV10_2_HRDINABUILDER_H
#include "Hrdina.h"

class HrdinaBuilder {
protected:
    Hrdina* m_hrdina;
public:
    HrdinaBuilder();
    void vytvorHrdinu();
    Hrdina* getHrdina();
    virtual void setZbran() = 0;
    virtual void setBrneni() = 0;
};


#endif //CV10_2_HRDINABUILDER_H
