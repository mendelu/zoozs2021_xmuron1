//
// Created by xmuron1 on 25.11.2021.
//

#ifndef CV10_2_BOHATYHRDINABUILDER_H
#define CV10_2_BOHATYHRDINABUILDER_H
#include "HrdinaBuilder.h"

class BohatyHrdinaBuilder : public HrdinaBuilder{
public:
    BohatyHrdinaBuilder();
    void setZbran() override;
    void setBrneni() override;
};


#endif //CV10_2_BOHATYHRDINABUILDER_H
