//
// Created by xmuron1 on 25.11.2021.
//

#include "BohatyHrdinaBuilder.h"

BohatyHrdinaBuilder::BohatyHrdinaBuilder(): HrdinaBuilder() {}

void BohatyHrdinaBuilder::setBrneni() {
    m_hrdina->seberBrneni(new Brneni(100));
}

void BohatyHrdinaBuilder::setZbran() {
    m_hrdina->seberZbran(new Zbran(100));
}