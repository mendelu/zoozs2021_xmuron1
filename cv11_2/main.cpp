#include <iostream>
#include "Hrdina.h"
#include "PrikazVypijPulku.h"
#include "PrikazVypisVse.h"

int main() {
    Hrdina* h = new Hrdina(50);
    h->seberLektvar(new Lektvar("Pivo",10));
    h->seberLektvar(new Lektvar("Voda",100));

    h->naucSePrikaz(new PrikazVypijPulku());
    h->naucSePrikaz(new PrikazVypisVse());

    h->provedAkciSLektvarem();

    std::cout << h->getZivoty() << std::endl;
    return 0;
}
