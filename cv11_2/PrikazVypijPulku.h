//
// Created by xmuron1 on 02.12.2021.
//

#ifndef CV11_2_PRIKAZVYPIJPULKU_H
#define CV11_2_PRIKAZVYPIJPULKU_H
#include "Prikaz.h"

class PrikazVypijPulku : public Prikaz {
public:
    PrikazVypijPulku();
    void pouziLektvar(Hrdina* hrdina, Lektvar* lektvar);
};


#endif //CV11_2_PRIKAZVYPIJPULKU_H
