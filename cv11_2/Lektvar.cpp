//
// Created by xmuron1 on 02.12.2021.
//

#include "Lektvar.h"

Lektvar::Lektvar(std::string jmeno, int bonus) {
    m_jmeno = jmeno;
    m_bonus = bonus;
}

int Lektvar::getBonus() {
    return m_bonus;
}

void Lektvar::setBonus(int bonus) {
    m_bonus = bonus;
}

void Lektvar::printInfo() {
    std::cout << "Lektvar " << m_jmeno << " s bonusem " << m_bonus << std::endl;
}