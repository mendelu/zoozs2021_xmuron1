//
// Created by xmuron1 on 02.12.2021.
//

#ifndef CV11_2_PRIKAZVYPISVSE_H
#define CV11_2_PRIKAZVYPISVSE_H
#include "Prikaz.h"

class PrikazVypisVse : public Prikaz {
public:
    PrikazVypisVse();
    void pouziLektvar(Hrdina* hrdina, Lektvar* lektvar);
};


#endif //CV11_2_PRIKAZVYPISVSE_H
