//
// Created by xmuron1 on 02.12.2021.
//

#ifndef CV11_2_LEKTVAR_H
#define CV11_2_LEKTVAR_H
#include <iostream>

class Lektvar {
private:
    int m_bonus;
    std::string m_jmeno;
public:
    Lektvar(std::string jmeno, int bonus);
    int getBonus();
    void setBonus(int bonus);
    void printInfo();
};


#endif //CV11_2_LEKTVAR_H
