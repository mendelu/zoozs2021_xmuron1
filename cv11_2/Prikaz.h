//
// Created by xmuron1 on 02.12.2021.
//

#ifndef CV11_2_PRIKAZ_H
#define CV11_2_PRIKAZ_H
#include <iostream>
#include "Hrdina.h"

class Prikaz {
private:
    std::string m_popis;
public:
    Prikaz(std::string popis);
    std::string getPopis();
    virtual void pouziLektvar(Hrdina* hrdina, Lektvar* lektvar)=0;
};


#endif //CV11_2_PRIKAZ_H
