//
// Created by xmuron1 on 02.12.2021.
//

#include "PrikazVypisVse.h"

PrikazVypisVse::PrikazVypisVse() : Prikaz("Vypije cely lektvar najednou") {}


void PrikazVypisVse::pouziLektvar(Hrdina *hrdina, Lektvar *lektvar) {
    hrdina->pridejZivoty(lektvar->getBonus());
    lektvar->setBonus(0);
}