//
// Created by xmuron1 on 02.12.2021.
//

#ifndef CV11_2_HRDINA_H
#define CV11_2_HRDINA_H
#include "Lektvar.h"
#include <iostream>
#include <vector>

class Prikaz;

class Hrdina {
private:
    int m_zivoty;
    std::vector<Lektvar*> m_lektvary;
    std::vector<Prikaz*> m_prikazy;
public:
    Hrdina(int zivoty);
    int getZivoty();
    void pridejZivoty(int kolik);
    void seberLektvar(Lektvar* lektvar);
    void naucSePrikaz(Prikaz* prikaz);
    void provedAkciSLektvarem();
};


#endif //CV11_2_HRDINA_H
