//
// Created by xmuron1 on 02.12.2021.
//

#include "PrikazVypijPulku.h"
PrikazVypijPulku::PrikazVypijPulku() : Prikaz("Vypije pulku lektvaru") {}

void PrikazVypijPulku::pouziLektvar(Hrdina *hrdina, Lektvar *lektvar) {
    int pulka = lektvar->getBonus() / 2;
    hrdina->pridejZivoty(pulka);
    lektvar->setBonus(pulka);
}