//
// Created by xmuron1 on 02.12.2021.
//

#include "Hrdina.h"
#include "Prikaz.h"

Hrdina::Hrdina(int zivoty) {
    m_zivoty = zivoty;
}

int Hrdina::getZivoty() {
    return m_zivoty;
}
void Hrdina::pridejZivoty(int kolik) {
    m_zivoty += kolik;
}

void Hrdina::seberLektvar(Lektvar *lektvar) {
    m_lektvary.push_back(lektvar);
}

void Hrdina::naucSePrikaz(Prikaz *prikaz) {
    m_prikazy.push_back(prikaz);
}


void Hrdina::provedAkciSLektvarem() {
    std::cout << "Napis, jaky lektvar chces pouzit" << std::endl;
    int i = 0;
    for(Lektvar* l : m_lektvary){
        std::cout << i << ")";
        l->printInfo();
        i += 1;
    }
    int index_lektvaru;
    std::cin >> index_lektvaru;
    // TODO kontrola velikosti indexu
    Lektvar* lektvar = m_lektvary[index_lektvaru];

    std::cout << "Napis, jaky prikaz chces pouzit" << std::endl;
    int j = 0;
    for(Prikaz* p : m_prikazy){
        std::cout << j << ")" << p->getPopis() << std::endl;
        j += 1;
    }
    int index_prikazu;
    std::cin >> index_prikazu;
    // TODO kontrola velikosti indexu
    Prikaz* prikaz = m_prikazy[index_prikazu];

    prikaz->pouziLektvar(this,lektvar);

}