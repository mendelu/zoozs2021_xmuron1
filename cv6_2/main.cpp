#include <iostream>
#include <array>
using namespace std;

class Kontejner{
private:
    int m_vaha;
    string m_obsah;
    string m_majitel;
public:
    Kontejner(int vaha, string obsah, string majitel){
        // TODO kontrola hodnot
        m_vaha = vaha;
        m_obsah = obsah;
        m_majitel = majitel;
    }
    int getVaha(){
        return m_vaha;
    }
    string getObsah(){
        return m_obsah;
    }
    string getMajitel(){
        return m_majitel;
    }
    void printInfo(){
        cout << "Jsem kontejner s obsahem " << getObsah() << " vazim " << getVaha() << " a vlastnikem je " << getMajitel() << endl;
    }
};

class Patro{
private:
    string m_oznaceni;
    array<Kontejner*,5> m_kontejnery;
public:
    Patro(string oznaceni){
        m_oznaceni = oznaceni;
        for(Kontejner* &k:m_kontejnery){
            k = nullptr;
        }
    }

    void uloz(Kontejner* k, int index){
        if(index < 5) {
            if(m_kontejnery[index] == nullptr){
                m_kontejnery[index] = k;
            }
            else{
                cout << "Na pozici uz kontejner je, nelze ulozit." << endl;
            }
        }
    }

    void odstran(int index){
        if(m_kontejnery[index] != nullptr) {
            m_kontejnery[index] = nullptr;
        }
        else{
            cout << "Nelze odstranit, na dane pozici nic neni." << endl;
        }
    }

    void vypisObsah(){
        for(Kontejner* k:m_kontejnery){
            if(k == nullptr){
                cout << "Na dane pozici neni zadny kontejner" << endl;
            }
            else {
                k->printInfo();
            }
        }
    }
};

class Sklad{
private:
    array<Patro*,10> m_patra;
public:
    Sklad(){
        for(Patro* &p:m_patra){
            // TODO pridat dynamicke oznaceni
            p = new Patro("A");
        }
    }

    ~Sklad(){
        for(Patro* &p:m_patra){
            delete p;
        }
    }

    void ulozKontejner(int index_patra, int index_pozice, Kontejner* k){
        // TODO kotroly indexu
        m_patra[index_patra]->uloz(k,index_pozice);
    }

    void odeberKontejner(int index_patra, int index_pozice){
        // TODO kotroly indexu
        m_patra[index_pozice]->odstran(index_pozice);
    }

    void vypisObsah(){
        // TODO hezi vypis
        for(Patro* p:m_patra){
            p->vypisObsah();
        }
    }
};

int main() {
    Kontejner* k1 = new Kontejner(1000,"odpadky","Mikuluas");
    Kontejner* k2 = new Kontejner(1000,"auta","Mikuluas");
    Sklad* s = new Sklad();
    s->ulozKontejner(0,0,k1);
    s->ulozKontejner(1,0,k2);
    s->vypisObsah();
    return 0;
}
