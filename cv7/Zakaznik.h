//
// Created by xmuron1 on 04.11.2021.
//

#ifndef CV7_ZAKAZNIK_H
#define CV7_ZAKAZNIK_H
#include <iostream>

class Zakaznik {
private:
    std::string m_jmeno;
    int m_id;
public:
    Zakaznik(int id, std::string jmeno);
    int getId();
    std::string getJmeno();

};


#endif //CV7_ZAKAZNIK_H
