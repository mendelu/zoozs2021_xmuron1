//
// Created by xmuron1 on 04.11.2021.
//

#ifndef CV7_SPRAVCEZAKAZNIKU_H
#define CV7_SPRAVCEZAKAZNIKU_H
#include "Zakaznik.h"
#include <vector>

class SpravceZakazniku {
private:
    static std::vector<Zakaznik*> s_zakaznici;
    static int s_pocet_zakazniku;
public:
    static Zakaznik* getById(int id);
    static std::vector<Zakaznik*> najdiPodleJmena(std::string jmeno);
    static Zakaznik* createZakaznik(std::string jmeno);
};


#endif //CV7_SPRAVCEZAKAZNIKU_H
