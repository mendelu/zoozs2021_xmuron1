//
// Created by xmuron1 on 04.11.2021.
//

#include "SpravceZakazniku.h"

std::vector<Zakaznik*> SpravceZakazniku::s_zakaznici = {};
int SpravceZakazniku::s_pocet_zakazniku = 0;

Zakaznik* SpravceZakazniku::getById(int id) {
    for(Zakaznik* z:s_zakaznici){
        if(z->getId() == id){
            return z;
        }
    }
    return createZakaznik("Anonym");
}

std::vector<Zakaznik*> SpravceZakazniku::najdiPodleJmena(std::string jmeno) {
    std::vector<Zakaznik*> nalezeni_zakaznici;
    for(Zakaznik* z:s_zakaznici){
        if(z->getJmeno() == jmeno){
            nalezeni_zakaznici.push_back(z);
        }
    }
    return nalezeni_zakaznici;
}

Zakaznik * SpravceZakazniku::createZakaznik(std::string jmeno) {
    Zakaznik* novy_zakaznik = new Zakaznik(s_pocet_zakazniku,jmeno);
    s_pocet_zakazniku += 1;
    s_zakaznici.push_back(novy_zakaznik);
    return novy_zakaznik;
}