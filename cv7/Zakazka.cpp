//
// Created by xmuron1 on 04.11.2021.
//

#include "Zakazka.h"


Zakazka::Zakazka(std::string popis, int id_zakaznika) {
    m_cena = 0;
    m_popis = popis;
    m_zakaznik = SpravceZakazniku::getById(id_zakaznika);
}

int Zakazka::getCena() {
    return m_cena;
}

std::string Zakazka::getPopis() {
    return m_popis;
}

void Zakazka::setCena(int cena) {
    m_cena = cena;
}