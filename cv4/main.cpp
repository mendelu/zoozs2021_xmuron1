#include <iostream>

using namespace std;

class Drak{
private:
    int m_zivoty;
    int m_sila;
    int m_obrana;
public:
    Drak(int sila, int obrana){
        m_sila = sila;
        m_obrana = obrana;
        m_zivoty = 100;
    }

    int getSila(){
        return m_sila;
    }

    int getObrana(){
        return m_obrana;
    }

    int getZivoty(){
        return m_zivoty;
    }

    void uberZivoty(int kolik){
        if(kolik) {
            m_zivoty -= kolik;
            if (m_zivoty <= 0) {
                m_zivoty = 0;
                cout << "Drak uz je mrtvy." << endl;
            }
        }
    }
};

class Rytir{
private:
    int m_zivoty;
    int m_sila;
    int m_obrana;
    string m_jmeno;

public:
    Rytir(string jmeno, int sila, int obrana){
        m_jmeno = jmeno;
        m_sila = sila;
        m_obrana = obrana;
        m_zivoty = 100;
    }

    int getSila(){
        return m_sila;
    }

    int getObrana(){
        return m_obrana;
    }

    int getZivoty(){
        return m_zivoty;
    }

    string getJmeno(){
        return m_jmeno;
    }

    void uberZivoty(int kolik){
        if(kolik > 0) {
            m_zivoty -= kolik;
            if (m_zivoty <= 0) {
                m_zivoty = 0;
                cout << "Rytir " << m_jmeno << " je mrtvy." << endl;
            }
        }
    }

    void boj(Drak* nepritel){
        if(nepritel->getZivoty() == 0){
            cout << "Nelze utocit na mrtvoly." << endl;
        }
        else {
            // ubirat nepriteli
            int kolik_ubrat = getSila() - nepritel->getObrana();
            nepritel->uberZivoty(kolik_ubrat);

            // ubrat sobe
            uberZivoty(nepritel->getSila() - getObrana());
        }
    }

};


int main() {
    Drak* zlyDrak = new Drak(10,10);
    Rytir* artus = new Rytir("Artus!", 2000,20);
    artus->boj(zlyDrak);
    cout << zlyDrak->getZivoty() << endl;
    artus->boj(zlyDrak);
    return 0;
}
