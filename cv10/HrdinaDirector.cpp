//
// Created by xmuron1 on 25.11.2021.
//

#include "HrdinaDirector.h"
HrdinaDirector::HrdinaDirector(HrdinaBuilder *builder) {
    m_builder = builder;
}

void HrdinaDirector::setBuilder(HrdinaBuilder *builder) {
    m_builder = builder;
}

Hrdina * HrdinaDirector::postavHrdinu() {
    m_builder->vytvorHrdinu();
    m_builder->setZbran();
    m_builder->setHelma();
    return m_builder->getHrdina();
}