//
// Created by xmuron1 on 25.11.2021.
//

#ifndef CV10_HRDINA_H
#define CV10_HRDINA_H

#include "Zbran.h"
#include "Helma.h"



class Hrdina {
private:
    int m_utok;
    int m_zivoty;
    Zbran* m_zbran;
    Helma* m_helma;
public:
    Hrdina(int utok);
    void setHelma(Helma* helma);
    int getObrana();
    void setZbran(Zbran* zbran);
    int getUtok();
};


#endif //CV10_HRDINA_H
