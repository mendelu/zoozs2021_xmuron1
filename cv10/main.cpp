#include <iostream>
#include "HrdinaDirector.h"
#include "LehkoOdenecBuilder.h"
#include "TezkoOdenecBuilder.h"

int main() {
    HrdinaDirector* director = new HrdinaDirector(new LehkoOdenecBuilder());
    Hrdina* h1 = director->postavHrdinu();
    Hrdina* h2 = director->postavHrdinu();

    director->setBuilder(new TezkoOdenecBuilder());
    Hrdina* h3 = director->postavHrdinu();
    return 0;
}
