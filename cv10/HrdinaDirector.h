//
// Created by xmuron1 on 25.11.2021.
//

#ifndef CV10_HRDINADIRECTOR_H
#define CV10_HRDINADIRECTOR_H
#include "HrdinaBuilder.h"

class HrdinaDirector {
private:
    HrdinaBuilder* m_builder;
public:
    HrdinaDirector(HrdinaBuilder* builder);
    void setBuilder(HrdinaBuilder* builder);
    Hrdina* postavHrdinu();
};


#endif //CV10_HRDINADIRECTOR_H
