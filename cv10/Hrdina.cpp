//
// Created by xmuron1 on 25.11.2021.
//

#include "Hrdina.h"

Hrdina::Hrdina(int utok) {
    m_utok = utok;
    m_zivoty = 100;
    m_helma = nullptr;
    m_zbran = nullptr;
}

void Hrdina::setHelma(Helma *helma) {
    m_helma = helma;
}

void Hrdina::setZbran(Zbran *zbran) {
    m_zbran = zbran;
}

int Hrdina::getUtok() {
    int utok = m_utok;
    if(m_zbran != nullptr){
        utok+= m_zbran->getBonusUtok();
    }
    return utok;
}

int Hrdina::getObrana() {
    if(m_helma != nullptr){
        return m_helma->getBonusObrana();
    }
    return 0;
}