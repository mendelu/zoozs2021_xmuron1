//
// Created by xmuron1 on 25.11.2021.
//

#ifndef CV10_HELMA_H
#define CV10_HELMA_H


class Helma {
private:
    int m_bonus_obrana;
public:
    Helma(int bonus);
    int getBonusObrana();
};


#endif //CV10_HELMA_H
