//
// Created by xmuron1 on 25.11.2021.
//

#ifndef CV10_LEHKOODENECBUILDER_H
#define CV10_LEHKOODENECBUILDER_H
#include "HrdinaBuilder.h"

class LehkoOdenecBuilder : public HrdinaBuilder {
public:
    LehkoOdenecBuilder();
    void setZbran() override;
    void setHelma() override;
};


#endif //CV10_LEHKOODENECBUILDER_H
