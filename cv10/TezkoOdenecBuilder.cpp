//
// Created by xmuron1 on 25.11.2021.
//

#include "TezkoOdenecBuilder.h"
TezkoOdenecBuilder::TezkoOdenecBuilder() : HrdinaBuilder() {

}

void TezkoOdenecBuilder::setHelma() {
    m_hrdina->setHelma(new Helma(100));
}

void TezkoOdenecBuilder::setZbran() {
    m_hrdina->setZbran(new Zbran(100));
}