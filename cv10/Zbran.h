//
// Created by xmuron1 on 25.11.2021.
//

#ifndef CV10_ZBRAN_H
#define CV10_ZBRAN_H


class Zbran {
private:
    int m_bonus_utok;
public:
    Zbran(int bonus);
    int getBonusUtok();
};


#endif //CV10_ZBRAN_H
