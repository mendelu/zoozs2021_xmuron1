//
// Created by xmuron1 on 25.11.2021.
//

#include "LehkoOdenecBuilder.h"
LehkoOdenecBuilder::LehkoOdenecBuilder() : HrdinaBuilder() {

}

void LehkoOdenecBuilder::setZbran() {
    m_hrdina->setZbran(new Zbran(10));
}

void LehkoOdenecBuilder::setHelma() {
    m_hrdina->setHelma(new Helma(1));
}