//
// Created by xmuron1 on 25.11.2021.
//

#ifndef CV10_TEZKOODENECBUILDER_H
#define CV10_TEZKOODENECBUILDER_H

#include "HrdinaBuilder.h"

class TezkoOdenecBuilder : public HrdinaBuilder{
public:
    TezkoOdenecBuilder();
    void setZbran() override;
    void setHelma() override;
};


#endif //CV10_TEZKOODENECBUILDER_H
